package me.moocar.logbackgelf;

import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.ListenableFuture;
import com.ning.http.client.Response;

import java.io.IOException;
import java.util.List;

/**
 * Responsible for sending packet(s) to the graylog2 server
 */
public class Transport {

    private final String graylog2GelfHttpUrl;

    public Transport(String graylog2GelfHttpUrl) {
        this.graylog2GelfHttpUrl = graylog2GelfHttpUrl;
    }

    /**
     * Sends a single packet GELF message to the graylog2 server
     *
     * @param data gzipped GELF Message (JSON)
     */
    public void send(byte[] data) {
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        ListenableFuture<Response> future = null;
        try {
            future = asyncHttpClient.preparePost(graylog2GelfHttpUrl).setBody(data).execute();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (future != null) {
                while (true) {
                    if (future.isDone()) {
                        asyncHttpClient.close();
                        break;
                    }
                }
            }
        }
    }

    /**
     * Sends a bunch of GELF Chunks to the graylog2 server
     *
     * @param packets The packets to send over the wire
     */
    public void send(List<byte[]> packets) {
        for(byte[] packet : packets) {
            send(packet);
        }
    }
}
